package pl.sda;

import pl.sda.model.Player;
import pl.sda.model.PlotState;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class Game {

    private final String playerName;
    private final BigDecimal startingCash;

    public Game(String playerName, BigDecimal startingCash) {
        this.playerName = playerName;
        this.startingCash = startingCash;
    }

    public void play() {
        Scanner scanner = new Scanner(System.in);
        final PlotState finale = PlotState.builder()
                .id("A")
                .preChoiceDescription("Proceed..")
                .description("Your empire grows, great work!")
                .build();

        final PlotState sellFood = PlotState.builder()
                .id("B")
                .description("Great choice! You will supply taco bars across galaxy!")
                .preChoiceDescription("Food")
                .possibleNextStates(Collections.singletonList(finale))
                .build();

        final PlotState sellComputers = PlotState.builder()
                .id("C")
                .preChoiceDescription("Computers")
                .description("Good choice! You create biggest IT company ever.")
                .possibleNextStates(Collections.singletonList(finale))
                .build();

        final PlotState merchantCruiser = PlotState.builder()
                .id("D")
                .preChoiceDescription("Select merchant cruiser")
                .description("You live happy and wealthy life as merchant. What do you want to sell?")
                .possibleNextStates(Arrays.asList(sellFood, sellComputers))
                .build();

        final PlotState stealthCruiser = PlotState.builder()
                .id("E")
                .preChoiceDescription("Select stealth cruiser")
                .description("Your cloaking systems are so good that nobody ever saw you..That's not the best finale :(")
                .possibleNextStates(Collections.emptyList())
                .build();

        final PlotState laser = PlotState.builder()
                .id("G")
                .preChoiceDescription("Laser")
                .description("Great choice! Fast and accurate weapon.")
                .possibleNextStates(Collections.singletonList(finale))
                .build();

        final PlotState minigun = PlotState.builder()
                .id("H")
                .preChoiceDescription("Minigun")
                .description("Great choice! Old school powerful weapon")
                .possibleNextStates(Collections.singletonList(finale))
                .build();

        final PlotState spaceFighter = PlotState.builder()
                .id("F")
                .preChoiceDescription("Select space fighter")
                .description("Choose your weapon:")
                .possibleNextStates(Arrays.asList(laser, minigun))
                .build();

        final PlotState start = PlotState.builder()
                .id("G")
                .description("Adventure awaits! Please select your spaceship type:")
                .possibleNextStates(Arrays.asList(merchantCruiser, stealthCruiser, spaceFighter))
                .build();

        final Player player = Player.builder()
                .name(playerName)
                .cash(BigDecimal.valueOf(1000L))
                .currentPlotState(start)
                .build();


        PlotState currentState = player.getCurrentPlotState();
        printStateDescriptionAndOptions(currentState);
        while (!player.getCurrentPlotState().isLast()) {
            final int choice = Integer.parseInt(scanner.nextLine()) - 1;
            final PlotState chosenState = currentState.getPossibleNextStates().get(choice);
            player.setCurrentPlotState(chosenState);
            currentState = player.getCurrentPlotState();
            printStateDescriptionAndOptions(currentState);
        }
    }

    private static void printStateDescriptionAndOptions(PlotState currentState) {
        System.out.println(currentState.getDescription());
        final Iterator<PlotState> stateIterator = currentState.getPossibleNextStates().iterator();
        int choiceNumber = 1;
        while (stateIterator.hasNext()) {
            final PlotState state = stateIterator.next();
            System.out.printf("%d. %s%n", choiceNumber++, state.getPreChoiceDescription());
        }
    }
}
