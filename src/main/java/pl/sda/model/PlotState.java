package pl.sda.model;

import java.util.Collections;
import java.util.List;

public class PlotState {
    private String id;
    private String description;
    private String preChoiceDescription;
    private List<PlotState> possibleNextStates;
/*    private List<PreCondition> conditions;
    private List<Event> possibleEvents;*/

    private PlotState() {
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public List<PlotState> getPossibleNextStates() {
        return possibleNextStates;
    }

    public boolean isLast() {
        return possibleNextStates.isEmpty();
    }


/*    public List<PreCondition> getConditions() {
        return conditions;
    }

    public List<Event> getPossibleEvents() {
        return possibleEvents;
    }*/

    public static PlotStateBuilder builder() {
        return new PlotStateBuilder();
    }

    public String getPreChoiceDescription() {
        return preChoiceDescription;
    }


    public static final class PlotStateBuilder {
        private String id;
        private String preChoiceDescription;
        private String description;
        private List<PlotState> possibleNextStates = Collections.emptyList();
/*        private List<PreCondition> conditions = Collections.emptyList();
        private List<Event> possibleEvents = Collections.emptyList();*/

        private PlotStateBuilder() {
        }

        public PlotStateBuilder id(String id) {
            this.id = id;
            return this;
        }

        public PlotStateBuilder preChoiceDescription(String preChoiceDescription) {
            this.preChoiceDescription = preChoiceDescription;
            return this;
        }

        public PlotStateBuilder description(String description) {
            this.description = description;
            return this;
        }

        public PlotStateBuilder possibleNextStates(List<PlotState> possibleNextStates) {
            this.possibleNextStates = possibleNextStates;
            return this;
        }

/*        public PlotStateBuilder conditions(List<PreCondition> conditions) {
            this.conditions = conditions;
            return this;
        }

        public PlotStateBuilder possibleEvents(List<Event> possibleEvents) {
            this.possibleEvents = possibleEvents;
            return this;
        }*/

        public PlotState build() {
            PlotState plotState = new PlotState();
            plotState.description = this.description;
            //plotState.conditions = this.conditions;
            plotState.id = this.id;
            plotState.preChoiceDescription = this.preChoiceDescription;
            plotState.possibleNextStates = this.possibleNextStates;
            //plotState.possibleEvents = this.possibleEvents;
            return plotState;
        }
    }
}
