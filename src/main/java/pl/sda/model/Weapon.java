package pl.sda.model;

public class Weapon {
    private String name;
    private int damagePerHit;

    public Weapon(String name, int damagePerHit) {
        this.name = name;
        this.damagePerHit = damagePerHit;
    }

    public String getName() {
        return name;
    }

    public int getDamagePerHit() {
        return damagePerHit;
    }
}
