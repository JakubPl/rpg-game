package pl.sda.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Player {
    private String name;
    private List<Weapon> weapons = new ArrayList<>();
    private BigDecimal cash;
    private double health;
    private PlotState currentPlotState;

    private Player() {
    }

    public BigDecimal getCash() {
        return cash;
    }

    public double getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public List<Weapon> getWeapons() {
        return weapons;
    }

    public PlotState getCurrentPlotState() {
        return currentPlotState;
    }

    public static PlayerBuilder builder() {
        return new PlayerBuilder();
    }

    public void setCurrentPlotState(PlotState chosenState) {
        this.currentPlotState = chosenState;
    }


    public static final class PlayerBuilder {
        private String name;
        private List<Weapon> weapons = new ArrayList<>();
        private BigDecimal cash;
        private double health;
        private PlotState currentPlotState;

        private PlayerBuilder() {
        }

        public PlayerBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PlayerBuilder weapons(List<Weapon> weapons) {
            this.weapons = weapons;
            return this;
        }

        public PlayerBuilder cash(BigDecimal cash) {
            this.cash = cash;
            return this;
        }

        public PlayerBuilder health(double health) {
            this.health = health;
            return this;
        }

        public PlayerBuilder currentPlotState(PlotState currentPlotState) {
            this.currentPlotState = currentPlotState;
            return this;
        }

        public Player build() {
            Player player = new Player();
            player.weapons = this.weapons;
            player.health = this.health;
            player.currentPlotState = this.currentPlotState;
            player.name = this.name;
            player.cash = this.cash;
            return player;
        }
    }
}
